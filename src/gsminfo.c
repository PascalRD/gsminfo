#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#include <limits.h>

#include <gsminfo.h>
#include <atmodem.h>

int main(int argc, char **argv)
{
    int fd;
    uint32_t optflags;
    char tty_path[PATH_MAX];
    modem_info_t modem_info;

    memset(modem_info.vendor_name, 0, sizeof(modem_info.vendor_name));
    memset(modem_info.model_name, 0, sizeof(modem_info.model_name));

    optflags = 0;
    modem_info.modem_type = ATMODEM;
    modem_info.flags = &optflags;

    parse_args(argc, argv, &optflags, tty_path);

    fd = open(tty_path, O_RDWR | O_NOCTTY | O_NDELAY);
    if (fd < 0)
    {
        fprintf(stderr, "Cannot open %s: %s\n",
                tty_path, strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (set_tty_options(fd, B115200) == -1)
    {
        exit(EXIT_FAILURE);
    }

    execute_at_cmd(ATDBGMSG, fd, NULL, 0); /* enable string errors */

    get_modem_info(fd, &modem_info);

    if (optflags & TESTFLAG)
    {
        run_action(fd, ACTIONPING, &modem_info);
    }

    if (optflags & HWIDFLAG)
    {
         printf("| %-22s| %s\n", "vendor:", modem_info.vendor_name);
         printf("| %-22s| %s\n", "model:",  modem_info.model_name);
    }

    if (optflags & SIGQFLAG)
    {
        run_action(fd, ACTIONSIGQUAL, &modem_info);
    }
    if (optflags & NETRFLAG)
    {
        run_action(fd, ACTIONAPNREG, &modem_info);
    }
    if (optflags & NETNFLAG)
    {
        run_action(fd, ACTIONAPNNAME, &modem_info);
    }
    if (optflags & APNNFLAG)
    {
        run_action(fd, ACTIONAPNNUM, &modem_info);
    }
    if (optflags & SIMPFLAG)
    {
        run_action(fd, ACTIONSIMSTAT, &modem_info);
    }
    if (optflags & TECHFLAG)
    {
        run_action(fd, ACTIONNETTECH, &modem_info);
    }

    close(fd);

    return EXIT_SUCCESS;
}
