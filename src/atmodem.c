#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>

#include <atmodem.h>

static char *get_error_message(const char *);
static void print_at_error(const char *, const char *);
static int parse_vendor_zte(const char *output);

static const char *net_tech[] = {
     "unknown",
     "2G [ GPRS ]",
     "2G [ EDGE ]",
     "3G [ WCDMA ]",
     "3G [ HSDPA ]",
     "3G [ HSUPA ]",
     "3G [ HSUPA and HSDPA ]",
     "2G [ GSM ]",
     "3G [ HSPA+ ]",
     "3G [ HSPA ]",
     "3G [ UMTS ]",
     NULL
};

static int parse_vendor_zte(const char *output)
{
    size_t  out_len = strlen(output);

    if (strncmp(output, "HSPA", out_len) == 0)
    {
         return TECH_HSPA;
    }
    else if (strncmp(output, "UMTS", out_len) == 0)
    {
         return TECH_UMTS;
    }
    else if (strncmp(output, "GSM", out_len) == 0 ||
             strncmp(output, "GPRS", out_len))
    {
         return TECH_GSM;
    }
    else if (strncmp(output, "HSDPA", out_len) == 0)
    {
         return TECH_HSDPA;
    }

    return TECH_NOTREG;
}

static void print_at_error(const char *output, const char *msg)
{
    char *err_msg = get_error_message(output);
    if (err_msg != NULL)
    {
        printf("| %-22s| Error: %s\n", msg, err_msg);
        free(err_msg);
    }
    else
    {
        printf("| %-22s| Error: Unknown\n", msg);
    }
}

static char *get_error_message(const char *output)
{
    const char *regexp = "\\+CME\\sERROR:\\s+((\\w\\s?)+)\\n+?";

    return parse_output(regexp, output);
}

ssize_t execute_at_cmd(const char *cmd, int fd, char *buf, size_t buf_len)
{
    ssize_t write_sz, read_sz;
    size_t cmd_len = strlen(cmd);
    char *bufptr = buf;

    if (buf == NULL)
    {
        write_sz = write(fd, cmd, cmd_len);
        usleep((cmd_len + ATTIMEOUT) * 100);
        return 0;
    }

    memset(buf, 0, buf_len);

    write_sz = write(fd, cmd, cmd_len);
    usleep((cmd_len + ATTIMEOUT) * 100);
    if (write_sz < 0)
    {
        fprintf(stderr, "write failed with error: %s\n",
                strerror(errno));
        return -1;
    }

    while ((read_sz = read(fd, bufptr, buf_len)) > 0)
    {
         buf_len -= read_sz;
         bufptr += read_sz;
         if (bufptr[-1] == '\n' || bufptr[-1] == '\r')
         {
              break;
         }
    }

    *bufptr = '\0';

    return read_sz;
}

void at_print_signal_quality(const char *output)
{
    const char *regexp = "\\+CSQ:\\s+([0-9]+),[0-9]+";
    int sig_level, dbm;
    char sig_qual[64];
    char *result = NULL;

    strncpy(sig_qual, GSMSIGUNKN, sizeof(sig_qual) - 1);

    if ((result = parse_output(regexp, output)) != NULL)
    {
        sig_level = atoi(result);

        if (sig_level >= 0 && sig_level < 10)
        {
            strncpy(sig_qual, GSMSIGBAD, sizeof(sig_qual) - 1);
        }
        else if (sig_level >= 10 && sig_level < 15)
        {
            strncpy(sig_qual, GSMSIGNORM, sizeof(sig_qual) - 1);
        }
        else if (sig_level >= 15 && sig_level < 20)
        {
            strncpy(sig_qual, GSMSIGGOOD, sizeof(sig_qual) - 1);
        }
        else if (sig_level >= 20 && sig_level <= 30)
        {
            strncpy(sig_qual, GSMSIGEXLNT, sizeof(sig_qual) - 1);
        }

        if (sig_level == 99)
        {
            dbm = 0;
        }
        else
        {
            dbm = -113 + (sig_level * 2);
        }

        printf("| %-22s| %d dBm [%s]\n", "signal strength:", dbm, sig_qual);
        free(result);
    }
    else
    {
        print_at_error(output, "signal strength:");
    }
}

void at_print_network_registration(const char *output)
{
    const char *regexp = "\\+CREG:\\s+[0-9],([0-9])\\s?";
    char *result = NULL;
    unsigned int reg_code;
    char reg_status[128];
    const char *reg_info[] = {
        GSMREGNOTMSG, GSMREGOKMSG, GSMREGSCANMSG,
        GSMREGDENYMSG, GSMREGUNKNMSG, GSMREGROAMMSG,
        GSMREGSMSHMSG, GSMREGSMSRMSG, GSMREGEMRGMSG,
        GSMREGSFBHMSG, GSMREGSFBRMSG
    };

    if ((result = parse_output(regexp, output)) != NULL)
    {
        reg_code = atoi(result);

        if (reg_code < ARRAY_COUNT(reg_info))
        {
            strncpy(reg_status, reg_info[reg_code], sizeof(reg_status) - 1);
        }
        else
        {
            strncpy(reg_status, GSMREGUNKNMSG, sizeof(reg_status) - 1);
        }

        printf("| %-22s| %s\n", "network registration:", reg_status);
        free(result);
    }
    else
    {
        print_at_error(output, "network registration:");
    }
}

void at_print_network_name(const char *output)
{
    const char *regexp = "\\+COPS:\\s+[0-9]+,[0-9]+,\"(.+)\"\\s+?";
    char *result = NULL;

    if ((result = parse_output(regexp, output)) != NULL)
    {
        printf("| %-22s| %s\n", "network name:", result);
        free(result);
    }
    else
    {
        print_at_error(output, "network name:");
    }
}

void at_print_network_num(const char *output)
{
    const char *regexp = "\\+COPS:\\s+[0-9]+,[0-9]+,\"([0-9]+)\"\\s+?";
    char *result = NULL;
    char mcc[4], mnc[4];

    memset(mcc, 0, sizeof(mcc));
    memset(mnc, 0, sizeof(mnc));

    if ((result = parse_output(regexp, output)) != NULL)
    {
        strncpy(mcc, result, 3);
        strncpy(mnc, result + 3, 3);

        printf("| %-22s| %s, %s\n", "mcc, mnc:", mcc, mnc);
        free(result);
    }
    else
    {
        print_at_error(output, "mcc, mnc:");
    }
}

void at_print_sim_pin(const char *output)
{
    const char *regexp = "\\+CPIN:\\s+(\\w+(\\s+P[A-Z]{2}[0-9]?)?)";
    char sim_status[128];
    char *result = NULL;

    memset(sim_status, 0, sizeof(sim_status));

    if ((result = parse_output(regexp, output)) != NULL)
    {
        size_t res_len = strlen(result);
        if (strncmp(result, GSMPINOK, res_len) == 0)
        {
            strncpy(sim_status, GSMSIMOKMSG, sizeof(sim_status) - 1);
        }
        else if (strncmp(result, GSMPINWAIT, res_len) == 0)
        {
            strncpy(sim_status, GSMPINWAITMSG, sizeof(sim_status) - 1);
        }
        else if (strncmp(result, GSMPIN2WAIT, res_len) == 0)
        {
            strncpy(sim_status, GSMPIN2WAITMSG, sizeof(sim_status) - 1);
        }
        else if (strncmp(result, GSMPUKWAIT, res_len) == 0)
        {
            strncpy(sim_status, GSMPUKWAITMSG, sizeof(sim_status) - 1);
        }
        else if (strncmp(result, GSMPUK2WAIT, res_len) == 0)
        {
            strncpy(sim_status, GSMPUK2WAITMSG, sizeof(sim_status) - 1);
        }
        else
        {
            strncpy(sim_status, GSMPINUNKMSG, sizeof(sim_status) - 1);
        }

        printf("| %-22s| %s\n", "sim card status:", sim_status);
        free(result);
    }
    else
    {
        print_at_error(output, "sim card status:");
    }
}

void at_print_net_tech(const char *output, int vendor)
{
    char regexp[256];
    int net_tech_num;
    char *result = NULL;

    memset(regexp, 0, sizeof(regexp));

    switch (vendor)
    {
        case VENDOR_ZTE:
            strncpy(regexp, "\\+ZPAS:\\s+\"(\\w+)\"", sizeof(regexp) - 1);
            break;
        default:
            strncpy(regexp, "\\+XREG:\\s+[0-9]+,([0-9]+),.*", sizeof(regexp) - 1);
    }

    if ((result = parse_output(regexp, output)) != NULL)
    {
        switch (vendor)
        {
             case VENDOR_ZTE:
                 net_tech_num = parse_vendor_zte(result);
                 break;
             default:
                 net_tech_num = atoi(result);
        }

        if (net_tech_num >= 0 && net_tech_num < 11)
        {
            printf("| %-22s| %s\n", "network tech:", net_tech[net_tech_num]);
        }
        else
        {
            printf("| %-22s| %s\n", "network tech:", "unknown");
        }
        free(result);
    }
    else
    {
        print_at_error(output, "network tech:");
    }
}

void at_get_vendor(const char *output, modem_info_t *info)
{
     const char *regexp = "AT\\+CGMI\\s+([a-zA-Z0-9-]+)";
     char *result = NULL;

     if ((result = parse_output(regexp, output)) != NULL)
     {
          size_t res_len = strlen(result);
          char l_name[128];

          memset(l_name, 0, sizeof(l_name));

          lower_word(result, l_name, res_len);

          if (strncmp(l_name, VENDOR_HUAWEI_NAME, res_len) == 0)
          {
              info->vendor = VENDOR_HUAWEI;
          }
          else if (strncmp(l_name, VENDOR_ZTE_NAME, res_len) == 0)
          {
              info->vendor = VENDOR_ZTE;
          }
          else
          {
              info->vendor = VENDOR_OTHER;
          }

          strncpy(info->vendor_name, result, sizeof(info->vendor_name) - 1);
          free(result);
     }
}

void at_get_product(const char *output, modem_info_t *info)
{
     const char *regexp = "AT\\+CGMM\\s+([a-zA-Z0-9-]+)";
     char *result = NULL;

     if ((result = parse_output(regexp, output)) != NULL)
     {
          strncpy(info->model_name, result, sizeof(info->model_name) - 1);
          free(result);
     }
}

void at_ping_modem(int fd, const uint32_t flags)
{
    size_t read_len;
    char buf[128];
    int rc = 1;
    const char *regexp = "AT\\s+(OK)";
    char *result = NULL;

    memset(buf, 0, sizeof(buf));

    read_len = execute_at_cmd(ATTEST, fd, buf, sizeof(buf));
    if (read_len > 0)
    {
        if ((result = parse_output(regexp, buf)) != NULL)
        {
             if (strncmp(result, "OK", 2) == 0)
             {
                 rc = 0;
             }
             free(result);
        }

        if (!(flags & SLNTFLAG))
        {
             if (rc == 0)
             {
                 printf("OK\n");
             }
             else
             {
                 fprintf(stderr, "ERROR\n");
             }
        }
    }

    exit(rc);
}
