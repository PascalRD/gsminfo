#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include <libgen.h>
#include <stdint.h>

#include <gsmoptions.h>

#ifndef VERSION
#define VERSION "0.1.3"
#endif

static void print_help(FILE *s, char *const prog)
{
    fprintf(s, "Usage: %s [options]\n%s\n", basename(prog),
                " -l line: modem path (e.g. /dev/ttyUSB0)\n"
                " -p: ping modem port\n"
                " -o: suppress output, use with \"-p\" option\n"
                " -d: print modem vendor and model\n"
                " -q: print signal strength\n"
                " -r: print network registration\n"
                " -n: print network name\n"
                " -c: print apn contry and network code\n"
                " -s: print SIM card status\n"
                " -t: print network technology\n"
                " -v: print version\n"
                " -h: print help and exit");
}

void parse_args(int argc, char *const argv[], uint32_t *flags, char *ttybuf)
{
    int opt, show_all;

    *flags = 0;
    show_all = 1;
    memset(ttybuf, 0, PATH_MAX);

    while ((opt = getopt(argc, argv, "opdtcvqrnshl:")) != -1)
    {
        switch (opt)
        {
            case 'l':
                *flags |= TTYPFLAG;
                strncpy(ttybuf, optarg, PATH_MAX - 1);
                break;
            case 'q':
                *flags |= SIGQFLAG;
                show_all = 0;
                break;
            case 'r':
                *flags |= NETRFLAG;
                show_all = 0;
                break;
            case 'n':
                *flags |= NETNFLAG;
                show_all = 0;
                break;
            case 's':
                *flags |= SIMPFLAG;
                show_all = 0;
                break;
            case 'c':
                *flags |= APNNFLAG;
                show_all = 0;
                break;
            case 't':
                *flags |= TECHFLAG;
                show_all = 0;
                break;
            case 'd':
                *flags |= HWIDFLAG;
                show_all = 0;
                break;
            case 'p':
                *flags |= TESTFLAG;
                show_all = 0;
                break;
            case 'o':
                *flags |= SLNTFLAG;
                break;
            case 'h':
                print_help(stdout, argv[0]);
                exit(EXIT_SUCCESS);
            case 'v':
                printf("version: %s\n", VERSION);
                exit(EXIT_SUCCESS);
            default:
                print_help(stderr, argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (show_all)
    {
        *flags |= ALLEFLAG;
    }

    if (!(*flags & TTYPFLAG))
    {
        strncpy(ttybuf, DEFAULT_TTY, PATH_MAX - 1);
    }
}
