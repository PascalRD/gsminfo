#ifndef GSMOPTIONS_H_
#define GSMOPTIONS_H_

#define DEFAULT_TTY "/dev/gsmmodem"

enum {
    TTYPFLAG   = 1,
    SIGQFLAG   = 1 << 1,
    NETRFLAG   = 1 << 2,
    NETNFLAG   = 1 << 3,
    SIMPFLAG   = 1 << 4,
    APNNFLAG   = 1 << 5,
    TECHFLAG   = 1 << 6,
    HWIDFLAG   = 1 << 7,
    TESTFLAG   = 1 << 8,
    SLNTFLAG   = 1 << 9,
    ALLEFLAG   = SIGQFLAG | NETRFLAG | NETNFLAG |
                 SIMPFLAG | APNNFLAG | TECHFLAG |
                 HWIDFLAG
};

void parse_args(int argc, char *const argv[], uint32_t *flags, char *ttybuf);

#endif /* GSMOPTIONS_H_ */
