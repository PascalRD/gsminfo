#ifndef GSMINFO_H_
#define GSMINFO_H_

#include <gsmcommon.h>
#include <gsmoptions.h>

enum {
     ACTIONPING,
     ACTIONSIGQUAL,
     ACTIONAPNNAME,
     ACTIONAPNNUM,
     ACTIONAPNREG,
     ACTIONSIMSTAT,
     ACTIONNETTECH
};

enum {
     TECH_NOTREG = 0,
     TECH_GPRS,
     TECH_EDGE,
     TECH_WCDMA,
     TECH_HSDPA,
     TECH_HSUPA,
     TECH_HSUPA_HSDPA,
     TECH_GSM,
     TECH_HSPA_PLUS,
     TECH_HSPA,
     TECH_UMTS
};

#endif /* GSMINFO_H_ */
