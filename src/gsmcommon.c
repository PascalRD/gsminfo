#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <termios.h>
#include <ctype.h>
#include <regex.h>

#include <gsmcommon.h>
#include <atmodem.h>

void *xmalloc(size_t size)
{
    void *ptr = NULL;

    ptr = malloc(size);
    if (ptr == NULL)
    {
        fprintf(stderr, "out of memory, need %lu butes", (unsigned long) size);
        exit(1);
    }

    return ptr;
}

void lower_word(const char *word, char *buf, size_t len)
{
    size_t n;

    for (n = 0; n < len; ++n)
    {
        buf[n] = tolower(word[n]);
    }
}

char *parse_output(const char *regexp, const char *output)
{
    char *result = NULL;
    regex_t reg;
    regmatch_t pmatch[2];
    size_t nmatch = 2;
    char output_copy[BUFSIZ];

    if (regcomp(&reg, regexp, REG_EXTENDED) != 0)
    {
        fprintf(stderr, "regcomp failed with error: %s\n",
                strerror(errno));
        exit(2);
    }

    if (regexec(&reg, output, nmatch, pmatch, 0) == 0)
    {
        size_t result_len = 0;
        strncpy(output_copy, output, sizeof(output_copy));
        output_copy[pmatch[1].rm_eo] = 0;
        result_len = strlen(output_copy + pmatch[1].rm_so);
        result = (char *) xmalloc(result_len + 1);
        strncpy(result, output_copy + pmatch[1].rm_so, result_len);
        result[result_len] = '\0';
    }

    regfree(&reg);

    return result;
}

int set_tty_options(int fd, int speed)
{
    struct termios tty;

    memset(&tty, 0, sizeof(tty));

    if (tcgetattr(fd, &tty) != 0)
    {
        fprintf(stderr, "Error get tty attr: %s\n",
                strerror(errno));
        return -1;
    }

    cfmakeraw(&tty);

    cfsetospeed(&tty, speed);
    cfsetispeed(&tty, speed);
    tty.c_cc[VMIN] = 0;
    tty.c_cc[VTIME] = 5;

    tcflush(fd, TCIOFLUSH);

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
    {
        fprintf(stderr, "Error set tty attr: %s\n",
                strerror(errno));
        return -1;
    }

    return 0;
}

void get_modem_info(int fd, modem_info_t *info)
{
    char buf[BUFSIZ];
    ssize_t read_sz;

    memset(buf, 0, sizeof(buf));

    switch (info->modem_type)
    {
        case ATMODEM:
            read_sz = execute_at_cmd(ATVENDOR, fd, buf, sizeof(buf));
            if (read_sz > 0)
            {
                at_get_vendor(buf, info);
            }
            else
            {
                info->vendor = VENDOR_OTHER;
                strncpy(info->vendor_name, "unknown", MODEM_INFO_LEN);
            }

            memset(buf, 0, sizeof(buf));
            
            read_sz = execute_at_cmd(ATMODEL, fd, buf, sizeof(buf));
            if (read_sz > 0)
            {
                at_get_product(buf, info);
            }
            else
            {
                 strncpy(info->model_name, "unknown", MODEM_INFO_LEN);
            }
            break;
    }
}

void run_action(int fd, int action, const modem_info_t *info)
{
    ssize_t read_sz;
    char buf[BUFSIZ];

    memset(buf, 0, sizeof(buf));

    switch (info->modem_type)
    {
         case ATMODEM:
              switch (action)
              {
                   case ACTIONPING:
                        at_ping_modem(fd, *(info->flags));
                        break;
                   case ACTIONSIGQUAL:
                        read_sz = execute_at_cmd(ATSIGQLT, fd, buf, sizeof(buf));
                        if (read_sz > 0)
                        {
                             at_print_signal_quality(buf);
                             return;
                        }
                        else goto err;
                        break;

                   case ACTIONAPNREG:
                        read_sz = execute_at_cmd(ATNETREG, fd, buf, sizeof(buf));
                        if (read_sz > 0) 
                        {
                             at_print_network_registration(buf);
                             return;
                        }
                        else goto err;
                        break;
                   
                   case ACTIONAPNNAME:
                        execute_at_cmd(ATAPNSTR, fd, NULL, 0); /* Set APN format to string */
                        read_sz = execute_at_cmd(ATOPNAME, fd, buf, sizeof(buf));
                        if (read_sz > 0)
                        {
                             at_print_network_name(buf);
                             return;
                        }
                        else goto err;
                        break;

                   case ACTIONAPNNUM:
                        execute_at_cmd(ATAPNNUM, fd, NULL, 0); /* Set APN format to numeric */
                        read_sz = execute_at_cmd(ATOPNAME, fd, buf, sizeof(buf));
                        if (read_sz > 0)
                        {
                             at_print_network_num(buf);
                             return;
                        }
                        else goto err;
                        break;

                   case ACTIONSIMSTAT:
                        read_sz = execute_at_cmd(ATSIMPIN, fd, buf, sizeof(buf));
                        if (read_sz > 0)
                        {
                             at_print_sim_pin(buf);
                             return;
                        }
                        else goto err;
                        break;

                   case ACTIONNETTECH:
                        switch (info->vendor)
                        {
                            case VENDOR_ZTE:
                                read_sz = execute_at_cmd(ATZTENETTEC, fd, buf, sizeof(buf));
                                break;
                            default:
                                read_sz = execute_at_cmd(ATNETTEC, fd, buf, sizeof(buf));
                        }
                        if (read_sz > 0)
                        {
                             at_print_net_tech(buf, info->vendor);
                             return;
                        }
                        else goto err;
                        break;
              }
    }
err:
     printf("No data received\n");
}
