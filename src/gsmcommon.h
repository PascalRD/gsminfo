#ifndef GSMCOMMON_H_
#define GSMCOMMON_H_

#include <stdint.h>
#include <gsminfo.h>

#define ARRAY_COUNT(arr) (sizeof(arr) / sizeof((arr)[0]))

#define GSMSIGBAD       "Bad"
#define GSMSIGNORM      "Normal"
#define GSMSIGGOOD      "Good"
#define GSMSIGEXLNT     "Excellent"
#define GSMSIGUNKN      "Unknown"

#define GSMREGOKMSG     "registered, home network"
#define GSMREGNOTMSG    "not registered"
#define GSMREGSCANMSG   "searching a new operator to register"
#define GSMREGDENYMSG   "registration denied"
#define GSMREGUNKNMSG   "unknown status"
#define GSMREGROAMMSG   "registered, roaming"
#define GSMREGSMSHMSG   "registered for \"SMS only\", home network"
#define GSMREGSMSRMSG   "registered for \"SMS only\", roaming"
#define GSMREGEMRGMSG   "attached for emergency bearer services only"
#define GSMREGSFBHMSG   "registered for \"CSFB not preferred\", home network"
#define GSMREGSFBRMSG   "registered for \"CSFB not preferred\", roaming"

#define GSMPINOK        "READY"
#define GSMPINWAIT      "SIM PIN"
#define GSMPIN2WAIT     "SIM PIN2"
#define GSMPUKWAIT      "SIM PUK"
#define GSMPUK2WAIT     "SIM PUK2"

#define GSMSIMOKMSG     "PIN code not required"
#define GSMPINWAITMSG   "waiting for SIM's first PIN code to be entered"
#define GSMPIN2WAITMSG  "waiting for SIM's second PIN code to be entered"
#define GSMPUKWAITMSG   "waiting for SIM's first PUK code to be entered"
#define GSMPUK2WAITMSG  "waiting for SIM's second PUK code to be entered"
#define GSMPINUNKMSG    "unknown status"

#define MODEM_INFO_LEN 256

#define VENDOR_HUAWEI_NAME "huawei"
#define VENDOR_ZTE_NAME    "zte"
#define VENDOR_OTHER_NAME  "other"

enum {
     ATMODEM = 0,
     QMIMODEM
};

enum {
     VENDOR_HUAWEI = 0,
     VENDOR_ZTE,
     VENDOR_OTHER
};

typedef struct {
     int vendor;
     int modem_type;
     char vendor_name[MODEM_INFO_LEN];
     char model_name[MODEM_INFO_LEN];
     const uint32_t *flags;
} modem_info_t;

int set_tty_options(int fd, int speed);
ssize_t execute_at_cmd(const char *cmd, int fd, char *buf, size_t buf_len);
void run_action(int fd, int action, const modem_info_t *info);
void get_modem_info(int fd, modem_info_t *info);

/**
 * @brief function parse string by regexp and return saved value.
 * Returned pointer should be freed by the user program.
 */
char *parse_output(const char *regexp, const char *output);

void *xmalloc(size_t size);
void lower_word(const char *word, char *buf, size_t len);

#endif /* GSMCOMMON_H_ */
