#ifndef ATMODEM_H_
#define ATMODEM_H_

#include <gsmcommon.h>

#define ATTIMEOUT 250

#define ATDBGMSG    "AT+CMEE=2\r\n"
#define ATSIGQLT    "AT+CSQ\r\n"
#define ATNETREG    "AT+CREG?\r\n"
#define ATOPNAME    "AT+COPS?\r\n"
#define ATSIMPIN    "AT+CPIN?\r\n"
#define ATAPNSTR    "AT+COPS=3,0\r\n"
#define ATAPNNUM    "AT+COPS=3,2\r\n"
#define ATNETTEC    "AT+XREG?\r\n"
#define ATZTENETTEC "AT+ZPAS?\r\n"
#define ATVENDOR    "AT+CGMI\r\n"
#define ATMODEL     "AT+CGMM\r\n"
#define ATTEST      "AT\r\n"

void at_print_signal_quality(const char *output);
void at_print_network_registration(const char *output);
void at_print_network_name(const char *output);
void at_print_network_num(const char *output);
void at_print_sim_pin(const char *output);
void at_print_net_tech(const char *output, int vendor);
void at_get_vendor(const char *output, modem_info_t *info);
void at_get_product(const char *output, modem_info_t *info);
void at_ping_modem(int fd, const uint32_t flags);

#endif /* ATMODEM_H_ */
