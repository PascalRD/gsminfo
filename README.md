# gsminfo #

## Overview ##
This little program shows information about GSM modems.

## Features ##
 * Shows modem vendor and model
 * Shows signal quality
 * Shows network registration
 * Shows network name
 * Shows SIM card status
 * Shows apn contry and network code
 * Shows network technology [limited]
 * Checks the modem port on the opportunity to enter AT commands

## Install ##

```
$ git clone https://bitbucket.org/PascalRD/gsminfo
$ cd gsminfo && mkdir build && cd build
$ cmake -DCMAKE_INSTALL_PREFIX=/usr ../
$ make
$ su
$ make install
```

## Example ##
```
./gsminfo
| vendor:               | Huawei
| model:                | E369
| signal strength:      | -83 dBm [Good]
| network registration: | registered, home network
| network name:         | OPSOS
| mcc, mnc:             | 250, 99
| sim card status:      | PIN code not required
| network tech:         | 3G [ HSUPA and HSDPA ]
```
